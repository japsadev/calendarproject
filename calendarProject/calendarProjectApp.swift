//
//  calendarProjectApp.swift
//  calendarProject
//
//  Created by japsa on 27.03.2024.
//

import SwiftUI

@main
struct calendarProjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
